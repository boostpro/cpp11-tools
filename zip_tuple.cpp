// Copyright Dave Abrahams 2011. Distributed under the Boost
// Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
#include <tuple>
#include <utility>
#include <iostream>

#include <boost/iterator/counting_iterator.hpp>

//
// Metafunction that returns the result type of doing the zip.
//
template<typename T1, typename T2> struct zip_type;

template<typename ...Elems1, typename ...Elems2>
struct zip_type<std::tuple<Elems1...>, std::tuple<Elems2...> >
{
    typedef std::tuple<std::pair<Elems1, Elems2>...> type;
};

//
// numbers is a compile-time container for a list of integral constants
//
template <class T, T ...n>
struct numbers
{
    // The nested type is a convenience that allows us to return
    // numbers from metafunctions by inheriting from instances.  The
    // return "value" of a metafunction is its nested ::type.
    typedef numbers type;
};

//
// The guts of the zip function.  We need to get ahold of a list of
// indices for the tuples, and dispatching to this function allows us
// to deduce it as a parameter pack.
//
template <typename ...E0, typename ...E1, unsigned ...indices>
auto zip_impl(
    std::tuple<E0...> const& t0,
    std::tuple<E1...> const& t1,
    numbers<unsigned, indices...>            // The value of this object isn't
                                   // used.  It's pure type
                                   // information
    )
    -> typename zip_type<std::tuple<E0...>, std::tuple<E1...> >::type
{
    // a tuple of pairs, where each pair is made up of element I of t0
    // and t1 for I = 0 ... <the length of the tuples>
    return std::make_tuple(
        std::make_pair(
            std::get<indices>(t0),
            std::get<indices>(t1)
            )...
        );
}

// see below
template <typename T, T start, T finish, T ...values>
struct range_impl;

//
// range - this is a metafunction that computes a specialization of
// numbers<...> above.  The arguments to the resulting specialization
// are values from start to finish increasing by 1.
//
// example: range<int, 3, 7>::type -> numbers<int, 3,4,5,6>
//
template <typename T, T start, T finish>
struct range
  // Inheritance is idiomatic metaprogramming here, known as
  // "metafunction forwarding."  The result of this metafunction is
  // the same as the result of the class from which it inherits
  // (dispatches to).
    : range_impl<T, start, finish>
{
    // For iterating over a range at runtime
    typedef boost::counting_iterator<T> iterator;
    constexpr iterator begin() { return iterator(start); }
    constexpr iterator end() { return iterator(finish); }
};

//
// range_impl<T,start,finish, ...values> - a metafunction yielding
// numbers<T, values... X...>, where X... are numbers from start to
// finish-1 inclusive.
//
template <typename T, T start, T finish, T ...values>
struct range_impl
  : range_impl<T,start+1,finish, values..., start>  // metafunction forwarding again.
{};

// Basis case for range_impl matches when start == finish
template <typename T, T finish, T ...values>
struct range_impl<T,finish, finish, values...>
  : numbers<T, values...> // stick the values accumulated so far into numbers<...>
// Every specialization of numbers<...> is a self-returning "nullary
// metafunction".  I could have written "using type =
// numbers<values...>" in the body instead of forwarding to
// numbers<...>
{};

//
// Finally the implementation of zip itself
//
// logically,
//
//   zip( {x, y, z}, {a, b, c} ) -> { {x, a}, {y, b}, {z, c} }
//
template <typename ...E0, typename ...E1>
constexpr auto zip(
    std::tuple<E0...> const& t0,
    std::tuple<E1...> const& t1
    )
    -> typename zip_type<std::tuple<E0...>, std::tuple<E1...> >::type
{
    // a specialization of numbers<unsigned, 0...N-1> where N is the
    // length of the tuple.
    using Indices = typename range<unsigned, 0,sizeof...(E0)>::type;

    // Construct an instance of Indices so zip_impl can deduce the
    // parameter pack.
    return zip_impl( t0, t1, Indices() );
}

auto t1 = std::make_tuple( 1, 2L, "foo" );
auto t2 = std::make_tuple( 2, 7, "bar" );

int main()
{
//    auto r = zip ( { 1, 2L, "foo" }, { 2, 7, "bar" } );

    // should print "foobar"
    std::cout
#if 0
        // shows that std::pair has a "tuple interface" now
        << std::get<0>(std::get<2>(zip(t1, t2)))
        << std::get<1>(std::get<2>(zip(t1, t2)))
#else 
        << std::get<2>(zip(t1, t2)).first
        << std::get<2>(zip(t1, t2)).second
#endif 
        << std::endl;

    for ( auto x : range<int, 3, 42>() )
        std::cout << x << " ";
    
    std::cout << std::endl;
}

