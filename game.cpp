// Copyright David Abrahams 2008. Distributed under the Boost
// Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#define _GLIBCXX_DEBUG 1

#include <functional>
#include <memory>
#include <set>
#include <iostream>

struct GameObject 
{
    GameObject(int points) : points(points), id(count++) {}
    virtual ~GameObject()
    {
        std::cout << "bye bye " << *this << std::endl;
    }
    virtual void time_slice() = 0;
    void damage(int d);


    friend std::ostream& operator<<(std::ostream& os, GameObject const& self)
    {
        return os << "object " << self.id;
    }
private:
    int points;

    // Just a way of identifying things.
    int id;
    static int count;
};
int GameObject::count = 0;

std::set< std::shared_ptr<GameObject> > all_targets;

void GameObject::damage(int d)
{
    std::cout << *this << " sustains " << d << " points of damage";
    if ( d > this->points )
    {
        std::cout << "...and is destroyed";
        all_targets.erase( $writeme$ );
    }
    else
    {
        this->points -= d;
    }
    std::cout << std::endl;
}

struct Tank : GameObject
{
    Tank() : GameObject(3000) {}

    ~Tank() {}
    virtual void time_slice()
    {
        std::shared_ptr<GameObject> p = this->target.lock();

        if (p)
            p->damage(500);
        else
            this->target = this->select_target();
    }

    std::shared_ptr<GameObject> select_target()
    {
        // Choose a random target.  Hint: if you shoot yourself that
        // makes the problem harder.  You might want to avoid that.
        $writeme$
    }

 private:
    std::weak_ptr<GameObject> target;
};


int main()
{
    for (int i = 0; i < 200; ++i)
    {
        std::shared_ptr<GameObject> p(new Tank);
        all_targets.insert(p);
    }

    while (all_targets.size() > 1)
    {
        std::for_each(
            all_targets.begin(), all_targets.end()
          , std::bind(&GameObject::time_slice,_1));
    }
    std::cout << **all_targets.begin() << " is the winner!" << std::endl;
}
