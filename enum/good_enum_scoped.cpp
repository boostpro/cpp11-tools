#include <iostream>
#include <climits>
#include <cassert>
#include <cstdlib>

enum class ShouldBeTiny : char { min, a, b, c, d, e, f, max };

class foo {
  ShouldBeTiny flag1;
  ShouldBeTiny flag2;
  ShouldBeTiny flag3;
  ShouldBeTiny flag4;
  ShouldBeTiny flag5;
  ShouldBeTiny flag6;
};

enum class System { Mac, Linux, Unix, Windows };

enum class IntValues : unsigned long {
  MinInt = 0x00000000,
  MaxInt = 0xFFFFFFFF
};

std::ostream& operator<<(std::ostream& out, const IntValues& value) {
  out << static_cast<unsigned long>(value);
  return out;
}

int main(int argc, char *[])
{
  foo tiny_object;
  assert(sizeof(foo) < 10);

  static const unsigned long Linux = ULONG_MAX;

  switch (System(argc)) {
  case System::Linux:
    std::cout << "This is a Linux machine." << std::endl;
  }
    
  std::cout << "Linux is " << Linux
            << "; size is " << sizeof(Linux) << std::endl;
  std::cout << "MaxInt is " << IntValues::MaxInt
            << "; size is " << sizeof(IntValues::MaxInt) << std::endl;

  assert(sizeof(Linux) == sizeof(IntValues::MaxInt));

  return 1;
}
