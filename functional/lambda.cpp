// Copyright David Abrahams 2008. Distributed under the Boost
// Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
#define _GLIBCXX_DEBUG 1

#include "boost/lambda/lambda.hpp"
#include "boost/lambda/if.hpp"
#include <algorithm>
#include <iostream>
#include <vector>
#include <cassert>
#include <numeric>

// Write one-line expressions to
// * Initialize a vector of integers with the numbers 0 … 99
// * Square the elements of a vector
// * Calculate the sum of squares of a vector
// * Print all even numbers in a vector of integers
// 
// Use only STL algorithms and function objects with Boost.Lambda
//
// Do not 
// * Use any explicitly written function objects
// * Write any explicit loop
//
// Bonus: How readable can you make the code?

int main()
{
    using namespace boost::lambda;
    std::vector<int> v(10);

    int i=0;
    
    // Initialize v with the numbers 0 - 9
    std::$some algorithm invocation$;
    std::copy(v.begin(), v.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    // Save a copy
    std::vector<int> v2(v);
    
    // Square the elements of v
    std::$some algorithm invocation$;
    std::copy(v.begin(), v.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    // Calculate the sum of squares of v2
    int ssqr = std::$some algorithm invocation$;
    assert(ssqr == 0*0 + 1*1 + 2*2 + 3*3 + 4*4 + 5*5 + 6*6 + 7*7 + 8*8 + 9*9);

    // Print all even numbers in v2
    //
    // Note: don't try to use std::endl in your lambda expression;
    // just stream '\n' if you must.
    std::$some algorithm invocation$;
    std::cout << std::endl;
}
