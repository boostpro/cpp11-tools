// Copyright David Abrahams 2008. Distributed under the Boost
// Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include "boost/range.hpp"
#include "boost/lambda/lambda.hpp"
#include "boost/iterator/counting_iterator.hpp"
#include "boost/iterator/filter_iterator.hpp"
#include <algorithm>
#include <numeric>
#include <functional>
#include <iostream>

template <class Range>
void init(Range& r)
{
    std::copy(
        boost::make_counting_iterator( typename boost::range_difference<Range>::type() ),
        boost::make_counting_iterator( boost::size( r ) ),
        boost::begin(r));
}

template <class Range>
void square(Range& r)
{
    using boost::lambda::_1;
    std::transform(boost::begin(r), boost::end(r), boost::begin(r), _1*_1);
}

template <class Range>
typename boost::range_value<Range>::type
sum_of_squares(Range& r)
{
    using boost::lambda::_1;
    using boost::lambda::_2;
    return std::accumulate(boost::begin(r), boost::end(r), typename boost::range_value<Range>::type(), _1 + _2*_2);
}

template <class Range>
void
print_evens(Range& r)
{
    using boost::lambda::_1;
    using boost::lambda::_2;
    
    std::for_each(
        boost::make_filter_iterator(_1 % 2 == 0, boost::begin(r), boost::end(r)),
        boost::make_filter_iterator(_1 % 2 == 0, boost::end(r), boost::end(r)),
        std::cout << _1 << " "
    );
    std::cout << std::endl;
}

int main()
{
    std::vector<int> v(100);
    init( v );
    square( v );
    int sum = sum_of_squares( v );
    assert(sum == 1950333330);
    std::cout << std::endl;
    print_evens( v );
}
