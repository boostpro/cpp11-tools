// Copyright David Abrahams 2008. Distributed under the Boost
// Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
#define _GLIBCXX_DEBUG 1

#include "boost/bind.hpp"
#include <algorithm>
#include <iostream>
#include <vector>

class image {};

class animation
{
public:
    explicit animation( bool active )
      : active(active), id(count++) {}

    void advance(int ms)
    {
        std::cout << "advancing animation #" << this->id
                  << " by " << ms << " ms" << std::endl;
    }
    
    bool inactive() const
    {
        return !this->active;
    }
    
    void render(image & target) const
    {
        std::cout << "rendering animation #" << this->id
                  << std::endl;
    }
    
 private:
    bool active;
    unsigned id;
    static unsigned count;
};

unsigned animation::count = 0;

std::vector<animation> anims;

template<class C, class P> 
void erase_if(C & c, P pred)
{
    c.erase(
        std::remove_if(c.begin(), c.end(), pred),
        c.end()
	);
}

void update(int ms)
{
    // use std::for_each and bind to advance 
    // each animation by ms
    std::for_each(anims.begin(), anims.end(), $writeme$ );

    // erase all inactive animations using erase_if
    erase_if( anims,  $writeme$ );
}

void render(image & target)
{
    // render each animation against target
    std::for_each(anims.begin(), anims.end(), 
                   $writeme$ );
}

int main()
{
    for (unsigned x = 0; x < 20; ++x)
    {
        anims.push_back( animation(x % 3 == 0) );
    }

    std::cout << "-- should advance animations 0-19 by 3 ms --" << std::endl;
    update(3);
    
    std::cout << "-- should render animations 0,3,6,9,12,15,18 --" << std::endl;
    image i;
    render(i);
}
