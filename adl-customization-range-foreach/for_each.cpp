// Copyright David Abrahams 2008. Distributed under the Boost
// Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include <boost/range.hpp>
#include <boost/foreach.hpp>
#include <iostream>
#include <vector>

template <class Range>
void init(Range& r)
{
    int i = 0;
    BOOST_FOREACH(typename boost::range_value<Range>::type& x, r)
    {
        x = i++;
    }
}

template <class Range>
void square(Range& r)
{
    BOOST_FOREACH(typename boost::range_value<Range>::type& x, r)
    {
        x *= x;
    }
}

template <class Range>
typename boost::range_value<Range>::type
sum_of_squares(Range& r)
{
    typename boost::range_value<Range>::type sum = 0;
    BOOST_FOREACH(typename boost::range_value<Range>::type x, r)
    {
        sum += x * x;
    }
    return sum;
}

template <class Range>
void
print_evens(Range& r)
{
    typename boost::range_value<Range>::type sum = 0;
    BOOST_FOREACH(typename boost::range_value<Range>::type x, r)
    {
        if (x % 2 == 0)
            std::cout << x << " ";
    }
    std::cout << std::endl;
}

int main()
{
    std::vector<int> v(100);
    init( v );
    square( v );
    int sum = sum_of_squares( v );
    assert(sum == 1950333330);
    std::cout << std::endl;
    print_evens( v );
}
