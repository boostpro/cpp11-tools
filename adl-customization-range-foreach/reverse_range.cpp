// Copyright David Abrahams 2008. Distributed under the Boost
// Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// * Write reverse_range<Iter>
//   * Constructors:
//     * reverse_range(Iter,Iter)
//     * reverse_range(Range) where Range’s iterators can be converted to Iter
//   * Provides two boost::reverse_iterators
// * Write function reverse(Range) -> reverse_range<Range’s iterator type>
// * Fill a vector v with consecutive values
// * Print them out using BOOST_FOREACH
// * Now print out the values in reverse(v)

#include "boost/iterator/reverse_iterator.hpp"
#include "boost/iterator/counting_iterator.hpp"
#include "boost/range.hpp"
#include "boost/foreach.hpp"
#include <vector>
#include <iostream>

namespace my {

// Write reverse_range<Iter>
//   Provides two boost::reverse_iterators
template <class Iter>
struct reverse_range
{
};

}

int main()
{
}
