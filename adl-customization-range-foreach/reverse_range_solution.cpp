// Copyright David Abrahams 2008. Distributed under the Boost
// Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// * Write reverse_range<Iter>
//   * Constructors:
//     * reverse_range(Iter,Iter)
//     * reverse_range(Range) where Range’s iterators can be converted to Iter
//   * Provides two boost::reverse_iterators
// * Write function reverse(Range) -> reverse_range<Range’s iterator type>
// * Fill a vector v with consecutive values
// * Print them out using BOOST_FOREACH
// * Now print out the values in reverse(v)

#define _GLIBCXX_DEBUG 1

#include "boost/iterator/reverse_iterator.hpp"
#include "boost/iterator/counting_iterator.hpp"
#include "boost/range.hpp"
#include "boost/foreach.hpp"
#include <vector>
#include <iostream>

namespace my {

// Write reverse_range<Iter>
//   Provides two boost::reverse_iterators
template <class Iter>
struct reverse_range
{
    //   Constructors:
    //     reverse_range(Iter,Iter)
    //     reverse_range(Range) where Range’s iterators can be converted to Iter
    reverse_range(Iter _start, Iter _finish)
      : start(_finish), finish(_start)
    {}

    // We don't want to copy r, so we must take it by reference...
    template <class Range>
    explicit reverse_range(Range& r)
      : start(boost::end(r)), finish(boost::begin(r))
    {}

    // ...but then that means we need another ctor to handle rvalues.
    template <class Range>
    explicit reverse_range(Range const& r)
      : start(boost::end(r)), finish(boost::begin(r))
    {}

    // Satisfy the Range concept requirements
    typedef boost::reverse_iterator<Iter> iterator;
    typedef boost::reverse_iterator<Iter> const_iterator;

    iterator begin() const
    {
        return this->start;
    }
    
    iterator end() const
    {
        return this->finish;
    }
    
 private:
    iterator start, finish;
};

// Write function reverse(Range) -> reverse_range<Range’s iterator type>
template <class Range>
reverse_range<typename boost::range_iterator<Range>::type>
reverse(Range& r)
{
    return reverse_range<typename boost::range_iterator<Range>::type>(r);
}

// Two overloads are needed by the same logic described for
// reverse_range's ctors
template <class Range>
reverse_range<typename boost::range_iterator<Range const>::type>
reverse(Range const& r)
{
    return reverse_range<typename boost::range_iterator<Range const>::type>(r);
}

}

int main()
{
    // Fill a vector v with consecutive values
    std::vector<int> v(boost::make_counting_iterator(0), boost::make_counting_iterator(100));

    // Print them out using BOOST_FOREACH
    BOOST_FOREACH(int x, v)
    {
        std::cout << x << " ";
    }
    std::cout << std::endl;

    // Now print out the values in reverse(v)
    BOOST_FOREACH(int x, my::reverse(v))
    {
        std::cout << x << " ";
    }
    std::cout << std::endl;
}
